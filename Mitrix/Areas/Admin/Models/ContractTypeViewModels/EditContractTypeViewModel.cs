﻿using System.ComponentModel.DataAnnotations;
using Mitrix.Models;

namespace Mitrix.Areas.Admin.Models.ContractTypeViewModels
{
    public class EditContractTypeViewModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        public bool Active { get; set; }

        public EditContractTypeViewModel()
        { }

        public EditContractTypeViewModel(ContractType contractType)
        {
            Id = contractType.Id;
            Name = contractType.Name;
            Active = contractType.Active;
        }

        public void UpdateContractTypeFromModel(ContractType contractType)
        {
            contractType.Name = Name;
            contractType.Active = Active;
        }
    }
}
