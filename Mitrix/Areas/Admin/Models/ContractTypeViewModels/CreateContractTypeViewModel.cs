﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Mitrix.Models;

namespace Mitrix.Areas.Admin.Models.ContractTypeViewModels
{
    public class CreateContractTypeViewModel
    {
        [Required]
        public string Name { get; set; }

        public bool Active { get; set; }

        public ContractType CreateContractTypeFromModel()
        {
            return new ContractType
            {
                Name = Name,
                Active = Active
            };
        }
    }
}
