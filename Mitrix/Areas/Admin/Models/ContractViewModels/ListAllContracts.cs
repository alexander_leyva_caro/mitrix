﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mitrix.Models;

namespace Mitrix.Areas.Admin.Models.ContractViewModels
{
    public class ListAllContracts
    {
        public IEnumerable<Tuple<ApplicationUser, Contract>> Contracts { get; set; }

        public string ContractType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
