﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Mitrix.Infraestructure.ValidationAttributes;
using Mitrix.Models;

namespace Mitrix.Areas.Admin.Models.ContractViewModels
{
    public class EditContractViewModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [DisplayName("Contract Type")]
        public string ContractTypeId { get; set; }

        [Required]
        [DateRangeValidation]
        public string DateRange { get; set; }

        public bool Active { get; set; }

        public string ClientId { get; set; }

        public SelectList ContractTypes { get; }

        public EditContractViewModel()
        { }

        public EditContractViewModel(Contract contract, string clientId, SelectList contractTypes)
        {
            Id = contract.Id;
            Name = contract.Name;
            ContractTypeId = contract.ContractTypeId;
            DateRange = $"{contract.StartDate:MM/dd/yyyy} - {contract.EndDate:MM/dd/yyyy}";
            Active = contract.Active;
            ClientId = clientId;
            ContractTypes = contractTypes;
        }

        // actions

        public void EditContractFromModel(Contract contract)
        {
            var dateRange = DateRangeValidationAttribute.ParseDateRange(DateRange);
            if (dateRange != null)
            {
                contract.StartDate = dateRange.Item1;
                contract.EndDate = dateRange.Item2;
            }

            contract.ContractTypeId = ContractTypeId;
            contract.Name = Name;
            contract.Active = Active;
        }
    }
}
