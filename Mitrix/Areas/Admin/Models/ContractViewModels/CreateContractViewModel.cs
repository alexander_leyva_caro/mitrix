﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Mitrix.Infraestructure.ValidationAttributes;
using Mitrix.Models;

namespace Mitrix.Areas.Admin.Models.ContractViewModels
{
    public class CreateContractViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [DisplayName("Contract Type")]
        public string ContractTypeId { get; set; }

        [Required]
        [DateRangeValidation]
        public string DateRange { get; set; }

        [Required]
        public string ClientId { get; set; }

        public bool Active { get; set; }

        public SelectList ContractTypes { get; set; }

        public CreateContractViewModel()
        { }

        public CreateContractViewModel(SelectList contractTypes)
        {
            ContractTypes = contractTypes;
        }

        // actions

        public Contract GeContractFromModel()
        {
            var dateRange = DateRangeValidationAttribute.ParseDateRange(DateRange);
            return new Contract
            {
                ClientId = ClientId,
                ContractTypeId = ContractTypeId,
                Name = Name,
                Active = Active,
                StartDate = dateRange.Item1,
                EndDate = dateRange.Item2
            };
        }
    }
}
