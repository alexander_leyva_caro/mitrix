﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Mitrix.Models;

namespace Mitrix.Areas.Admin.Models.ClientViewModels
{
    public class CreateClientViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        public string Company { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [DisplayName("Phone")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        public bool Active { get; set; }

        public ApplicationUser Create()
        {
            return new ApplicationUser
            {
                UserName = Email,
                Name = Name,
                Email = Email,
                Company = Company,
                Address = Address,
                Notes = Notes,
                PhoneNumber = PhoneNumber,
                Active = Active
            };
        }
    }
}
