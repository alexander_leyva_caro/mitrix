﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mitrix.Models;

namespace Mitrix.Areas.Admin.Models.ClientViewModels
{
    public class ListClientWithContractsViewModels
    {
        public ApplicationUser ApplicationUser { get; set; }
        public Contract Contract { get; set; }
    }
}
