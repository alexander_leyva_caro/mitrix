﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Mitrix.Areas.Admin.Models.ContractTypeViewModels;
using Mitrix.Models;

namespace Mitrix.Areas.Admin.Models.ClientViewModels
{
    public class EditClientViewModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        public string Company { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [DisplayName("Phone")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        
        public string Notes { get; set; }

        public bool Active { get; set; }

        public ApplicationUser Create()
        {
            return new ApplicationUser
            {
                UserName = Email,
                Name = Name,
                Email = Email,
                Company = Company,
                Address = Address,
                Notes = Notes,
                PhoneNumber = PhoneNumber,
                Active = Active
            };
        }

        public static EditClientViewModel Create(ApplicationUser client)
        {
            return new EditClientViewModel
            {
                Id = client.Id,
                Name = client.Name,
                Email = client.Email,
                Company = client.Company,
                Address = client.Address,
                PhoneNumber = client.PhoneNumber,
                Notes = client.Notes,
                Active = client.Active
            };
        }

        public void UpdateClientFromModel(ApplicationUser client)
        {
            client.Name = Name;
            client.Email = Email;
            client.Company = Company;
            client.Address = Address;
            client.PhoneNumber = PhoneNumber;
            client.Notes = Notes;
            client.Active = Active;
        }
    }
}
