﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Mitrix.Areas.Admin.Models.ContractTypeViewModels;
using Mitrix.Controllers;
using Mitrix.Data.Repositories;
using Mitrix.Data.Seeds;
using Mitrix.Models;

namespace Mitrix.Areas.Admin.Controllers
{
    [Area(SeedRoles.ADMIN_ROLE)]
    [Authorize(Roles = SeedRoles.ADMIN_ROLE)]
    public class ContractTypeController : AppController
    {
        private readonly IRepository<ContractType> _contractTypeRepo;

        public ContractTypeController(
            UserManager<ApplicationUser> userManager,
            IRepository<ContractType> contractTypeRepo)
            : base(userManager)
        {
            _contractTypeRepo = contractTypeRepo;
        }

        public ViewResult Index()
        {
            ViewBag.Header = "List Contracts Types";
            return View(_contractTypeRepo.FetchAll());
        }

        public ViewResult Create()
        {
            ViewBag.Header = "Create Contract Type";
            return View();
        }

        [HttpPost]
        public IActionResult Create(CreateContractTypeViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var contractType = model.CreateContractTypeFromModel();
            _contractTypeRepo.Create(contractType);
            SetNotification("Contract Type created");
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Edit(string id)
        {
            var contratType = _contractTypeRepo.Find(id);
            if (contratType == null)
            {
                SetNotification("Contract Type not found");
                return RedirectToAction(nameof(Index));
            }

            ViewBag.Header = "Editing Contract Type";
            var model = new EditContractTypeViewModel(contratType);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(EditContractTypeViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var contractType = _contractTypeRepo.Find(model.Id);
            if (contractType == null)
            {
                SetNotification("Contract Type not found");
                return RedirectToAction(nameof(Index));
            }

            model.UpdateContractTypeFromModel(contractType);
            _contractTypeRepo.Update(contractType);
            SetNotification("Contract Type Updated");
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult Delete(string id)
        {
            var contractType = _contractTypeRepo.Find(id);
            if (contractType != null)
            {
                _contractTypeRepo.Delete(contractType);
                SetNotification("Contract Type removed");
            }
            else
            {
                SetNotification("Contract Type not found");
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
