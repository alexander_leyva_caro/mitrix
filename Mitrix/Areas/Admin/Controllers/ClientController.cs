﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mitrix.Areas.Admin.Models.ClientViewModels;
using Mitrix.Areas.Admin.Models.ContractTypeViewModels;
using Mitrix.Controllers;
using Mitrix.Data.Seeds;
using Mitrix.Infraestructure.Extensions;
using Mitrix.Infraestructure.Services.EmailSender;
using Mitrix.Models;
using Mitrix.Models.ApplicationUserViewModels;

namespace Mitrix.Areas.Admin.Controllers
{
    [Area(SeedRoles.ADMIN_ROLE)]
    [Authorize(Roles = SeedRoles.ADMIN_ROLE)]
    public class ClientController : AppController
    {
        private readonly IUserValidator<ApplicationUser> _userValidator;
        private readonly IEmailSender _emailSender;

        public ClientController(
            UserManager<ApplicationUser> userManager,
            IUserValidator<ApplicationUser> userValidator,
            IEmailSender emailSender) 
            : base(userManager)
        {
            _userValidator = userValidator;
            _emailSender = emailSender;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.Header = "List Clients";
            var model = await GetClients();
            return View(model);
        }

        public async Task<IActionResult> Details(string id)
        {
            var client = await GetClient(id);
            if (client != null)
            {
                ViewBag.Header = $"Client: {client.Name}";
                return View(client);
            }

            SetNotification("Client not found");
            return RedirectToAction(nameof(Index));
        }

        public ViewResult Create()
        {
            ViewBag.Header = "Create Client";
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateClientViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var client = model.Create();
            var createClientIdentityResult = await UserManager.CreateAsync(client);
            if (!createClientIdentityResult.Succeeded)
            {
                AddErrorsFromResult(createClientIdentityResult);
                return View(model);
            }

            await UserManager.AddToRoleAsync(client, SeedRoles.CLIENT_ROLE);

            try
            {
                await PasswordResetLink(client, SetupPasswordType.NewClient, _emailSender);
                SetNotification("Client created");
            }
            catch (Exception)
            {
                SetNotification("Client Created but there was an error to save the email");  // warning
            }
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(string id)
        {
            var client = await GetClient(id);
            if (client != null)
            {
                ViewBag.Header = $"Editing Client: {client.Name}";
                var model = EditClientViewModel.Create(client);
                return View(model);
            }

            SetNotification("Client not found");
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditClientViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var client = await GetClient(model.Id);
            if (client != null)
            {
                model.UpdateClientFromModel(client);
                var identityResult = await _userValidator.ValidateAsync(UserManager, client);
                if (identityResult.Succeeded)
                {
                    identityResult = await UserManager.UpdateAsync(client);
                    if (identityResult.Succeeded)
                    {
                        SetNotification("Client Updated");
                        return RedirectToAction(nameof(Details), new { id = model.Id});
                    }
                }

                AddErrorsFromResult(identityResult);
                return View(model);
            }

            SetNotification("Client not found");
            return RedirectToAction(nameof(Index));
        }
    }
}
