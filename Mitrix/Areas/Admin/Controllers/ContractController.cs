﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Mitrix.Areas.Admin.Models.ClientViewModels;
using Mitrix.Areas.Admin.Models.ContractViewModels;
using Mitrix.Controllers;
using Mitrix.Data.Repositories;
using Mitrix.Data.Seeds;
using Mitrix.Infraestructure.ActionFilterAttributes;
using Mitrix.Infraestructure.Extensions;
using Mitrix.Models;

namespace Mitrix.Areas.Admin.Controllers
{
    [Area(SeedRoles.ADMIN_ROLE)]
    [Authorize(Roles = SeedRoles.ADMIN_ROLE)]
    public class ContractController : AppController
    {
        private readonly IRepository<ContractType> _contractTypeRepo;
        private readonly IRepository<Contract> _contractRepo;

        public ContractController(
            UserManager<ApplicationUser> userManager,
            IRepository<ContractType> contractTypeRepo,
            IRepository<Contract> contractRepo) 
            : base(userManager)
        {
            _contractTypeRepo = contractTypeRepo;
            _contractRepo = contractRepo;
        }

        public async Task<IActionResult> Index(string clientId)
        {
            var client = await GetClient(clientId);
            if (client != null)
            {
                ViewBag.Header = $"{client.Name} contracts";
                return View(client);
            }

            SetNotification("Client not found");
            return RedirectToAction(nameof(ClientController.Index), GetControllerName<ClientController>());
        }

        [HttpGet]
        public async Task<IActionResult> Create(string clientId)
        {
            var client = await GetClient(clientId);
            if (client != null)
            {
                ViewBag.Header = $"Create Contract for {client.Name}";
                var model = new CreateContractViewModel(GetSelectListContractTypes)
                {
                    ClientId = clientId
                };
                return View(model);
            }

            SetNotification("Client not found");
            return RedirectToAction(nameof(ClientController.Index), GetControllerName<ClientController>());
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateContractViewModel model, string clientId)
        {
            var client = await GetClient(model.ClientId);
            if (client != null)
            {
                if (!ModelState.IsValid)
                {
                    return await Create(clientId);
                }

                var contract = model.GeContractFromModel();

                // todo: we must move this validation to a validation attrbute model
                if (Contract.SomeContractOverlappingDates(client.Contracts, contract.StartDate, contract.EndDate))
                {
                    ModelState.AddModelError(nameof(CreateContractViewModel.DateRange), "Overlapping another contracts");
                    return await Create(clientId);
                }

                _contractRepo.Create(contract);
                return RedirectToAction(nameof(Index), new { clientId = contract.ClientId });
            }

            SetNotification("Client not found");
            return RedirectToAction(nameof(ClientController.Index), GetControllerName<ClientController>());
        }

        public ActionResult Edit(string id)
        {
            var contract = _contractRepo.FetchAll()
                .AsQueryable()
                .Include(item => item.Client)
                .FirstOrDefault(item => item.Id == id);
            if (contract != null)
            {
                ViewBag.Header = $"Edit Contract for {contract.Client.Name}";
                return View(new EditContractViewModel(contract, contract.ClientId, new SelectList(
                    _contractTypeRepo.FetchAll().Where(item => item.Active),
                    nameof(ContractType.Id),
                    nameof(ContractType.Name))));
            }

            SetNotification("Contract not found");
            return RedirectToAction(
                actionName: nameof(ClientController.Index),
                controllerName: nameof(ClientController).Replace("Controller", ""));
        }

        [HttpPost]
        public ActionResult Edit(EditContractViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Edit(model.Id);
            }

            var contract = _contractRepo.FetchAll()
                .AsQueryable()
                .Include(item => item.Client)
                .FirstOrDefault(item => item.Id == model.Id);
            model.EditContractFromModel(contract);

            // todo: we must move this validation to a validation attrbute model
            if (Contract.SomeContractOverlappingDates(contract.Client.Contracts, contract.StartDate, contract.EndDate))
            {
                ModelState.AddModelError(nameof(CreateContractViewModel.DateRange), "Overlapping another contracts");
                return Edit(model.Id);
            }

            _contractRepo.Update(contract);
            return RedirectToAction(
                actionName: nameof(Index),
                controllerName: nameof(ContractController).Replace("Controller", ""),
                routeValues: new { clientId = contract.ClientId });
        }

        public async Task<IActionResult> All(ListAllContracts model)
        {
            model.ContractType = model.ContractType?.ToLower().Trim();
            var startDate = model.StartDate ?? DateTime.Now;
            var endDate = model.EndDate ?? DateTime.MaxValue;

            model.Contracts = (await GetClients())
                .SelectMany(item => item.Contracts)
                .Where(contract =>
                {
                    if (model.ContractType != null && !contract.ContractType.Name.Contains(model.ContractType))
                        return false;

                    return contract.StartDate.IsInRange(startDate, endDate)
                           || contract.EndDate.IsInRange(startDate, endDate);
                })
                .Select(contract => Tuple.Create(contract.Client, contract));

            ViewBag.Header = "List All Contracts";
            return View(model);
        }

        private SelectList GetSelectListContractTypes
        {
            get
            {
                return new SelectList(_contractTypeRepo.FetchAll()
                    .Where(item => item.Active), nameof(ContractType.Id), nameof(ContractType.Name));
            }
        }
    }
}
