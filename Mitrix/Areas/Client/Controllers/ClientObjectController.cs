﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mitrix.Areas.Client.Models.ClientObjectViewModels;
using Mitrix.Controllers;
using Mitrix.Data.Repositories;
using Mitrix.Data.Seeds;
using Mitrix.Models;

namespace Mitrix.Areas.Client.Controllers
{
    [Area(SeedRoles.CLIENT_ROLE)]
    [Authorize(Roles = SeedRoles.CLIENT_ROLE)]
    public class ClientObjectController : AppController
    {
        private readonly IRepository<ClientObject> _clientObjectRepo;

        public ClientObjectController(
            UserManager<ApplicationUser> userManager,
            IRepository<ClientObject> clientObjectRepo) 
            : base(userManager)
        {
            _clientObjectRepo = clientObjectRepo;
        }

        public ViewResult Index()
        {
            ViewBag.Header = "List Client Objects";
            var currentUser = UserManager.Users
                .Include(item => item.ClientObjects)
                .FirstOrDefault(item => item.UserName == HttpContext.User.Identity.Name);
            return View(currentUser);
        }

        public ViewResult Create()
        {
            ViewBag.Header = "Create Client Object";
            return View(new CreateEditClientObjectViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateEditClientObjectViewModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var currentUser = await CurrentUser;
            var clientObject = model.GetNewClientObjectFromModel();
            clientObject.ClientId = currentUser.Id;
            _clientObjectRepo.Create(clientObject);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Details(string id)
        {
            var clientObject = _clientObjectRepo.Find(id);
            if (!await IsClientObjectOwner(clientObject))
            {
                return RedirectToAction(nameof(Index));
            }

            ViewBag.Header = $"Client Object {clientObject.Name} Details";
            return View(new CreateEditClientObjectViewModel(clientObject)
            {
                ReadOnly = true
            });
        }

        public async Task<IActionResult> Edit(string id)
        {
            var clientObject = _clientObjectRepo.Find(id);
            if (! await IsClientObjectOwner(clientObject))
            {
                return RedirectToAction(nameof(Index));
            }

            ViewBag.Header = $"Client Object {clientObject.Name} Details";
            return View(new CreateEditClientObjectViewModel(clientObject));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CreateEditClientObjectViewModel model)
        {
            var clientObject = _clientObjectRepo.Find(model.Id);
            if (!await IsClientObjectOwner(clientObject))
            {
                return RedirectToAction(nameof(Index));
            }

            model.UpdateClientObjectFromModel(clientObject);
            _clientObjectRepo.Update(clientObject);
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> IsClientObjectOwner(ClientObject clientObject)
        {
            if (clientObject == null)
            {
                SetNotification("Client Object Not Found");
                return false;
            }

            var currentUser = await CurrentUser;
            if (currentUser.Id != clientObject.ClientId)
            {
                SetNotification("You are not the Client Object owner");
                return false;
            }

            return true;
        }
    }
}
