﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Mitrix.Areas.Client.Models.ProfileViewModels;
using Mitrix.Controllers;
using Mitrix.Data.Seeds;
using Mitrix.Models;
using Mitrix.Models.ApplicationUserViewModels;

namespace Mitrix.Areas.Client.Controllers
{
    [Area(SeedRoles.CLIENT_ROLE)]
    [Authorize(Roles = SeedRoles.CLIENT_ROLE)]
    public class ProfileController : AppController
    {
        private readonly IUserValidator<ApplicationUser> _userValidator;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IPasswordValidator<ApplicationUser> _passwordValidator;

        public ProfileController(
            UserManager<ApplicationUser> userManager,
            IUserValidator<ApplicationUser> userValidator, 
            SignInManager<ApplicationUser> signInManager, 
            IPasswordValidator<ApplicationUser> passwordValidator)
            : base(userManager)
        {
            _userValidator = userValidator;
            _signInManager = signInManager;
            _passwordValidator = passwordValidator;
        }

        public async Task<ActionResult> Index()
        {
            ViewBag.Header = "Profile Details";
            return View(await CurrentUser);
        }

        public async Task<ActionResult> Edit()
        {
            ViewBag.Header = "Edit Profile";
            var currentUser = await CurrentUser;
            var model = new EditProfileViewModel(currentUser);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditProfileViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var client = await CurrentUser;
            model.EditClientFromModel(client);

            client.Email = model.Email;
            var emailValidation = await _userValidator.ValidateAsync(UserManager, client);
            if (emailValidation.Succeeded)
            {
                await UserManager.UpdateAsync(client);
                SetNotification("Profile Updated");
                return RedirectToAction(nameof(Index));
            }

            AddErrorsFromResult(emailValidation);
            return View(model);
        }

        public IActionResult ChangePassword()
        {
            return View(new ChangePasswordViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var currentUser = await CurrentUser;
            var passwordValidation = await _passwordValidator.ValidateAsync(UserManager, currentUser, model.NewPassword);
            if (passwordValidation.Succeeded)
            {
                var changePasswordResult = await UserManager.ChangePasswordAsync(currentUser, model.CurrentPassword, model.NewPassword);
                if (changePasswordResult.Succeeded)
                {
                    SetNotification("Password updated");
                    await _signInManager.SignInAsync(currentUser, isPersistent: false);
                    return RedirectToAction(nameof(Index));
                }
                AddErrorsFromResult(changePasswordResult);
            }
            AddErrorsFromResult(passwordValidation);
            return View(model);
        }
    }
}
