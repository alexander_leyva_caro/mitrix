﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mitrix.Areas.Client.Models.ProfileViewModels
{
    public class ChangePasswordViewModel
    {
        [Required]
        [DisplayName("Current Password")]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Required]
        [DisplayName("New Password")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required]
        [Compare(nameof(NewPassword))]
        [DisplayName("Confirm New Password")]
        [DataType(DataType.Password)]
        public string NewPasswordConfirmation { get; set; }
    }
}
