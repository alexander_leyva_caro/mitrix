﻿using System.ComponentModel.DataAnnotations;
using Mitrix.Models;

namespace Mitrix.Areas.Client.Models.ProfileViewModels
{
    public class EditProfileViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        public string Company { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string Address { get; set; }

        public EditProfileViewModel()
        { }

        public EditProfileViewModel(ApplicationUser client)
        {
            Name = client.Name;
            Email = client.Email;
            Company = client.Company;
            PhoneNumber = client.PhoneNumber;
            Address = client.Address;
        }

        public void EditClientFromModel(ApplicationUser client)
        {
            client.Name = Name;
            client.Company = Company;
            client.PhoneNumber = PhoneNumber;
            client.Address = Address;
        }
    }
}
