﻿using System.ComponentModel.DataAnnotations;
using Mitrix.Models;
using Mitrix.Models.ComponentsViewModels;

namespace Mitrix.Areas.Client.Models.ClientObjectViewModels
{
    public class CreateEditClientObjectViewModel
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Url]
        public string WebAddress { get; set; }

        public string Description { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public double Lat { get; set; }

        [Required]
        public double Lng { get; set; }

        public bool Active { get; set; }

        public bool ReadOnly { get; set; }

        public GeoLocationViewModel GeoLocationViewModel => new GeoLocationViewModel
        {
            Address = Address,
            Lat = Lat,
            Lng = Lng,
            ReadOnly = ReadOnly
        };

        public CreateEditClientObjectViewModel()
        { }

        public CreateEditClientObjectViewModel(ClientObject clientObject)
        {
            Id = clientObject.Id;
            Name = clientObject.Name;
            WebAddress = clientObject.WebAddress;
            Description = clientObject.Description;
            Country = clientObject.Country;
            Address = clientObject.Address;
            Lat = clientObject.Lat;
            Lng = clientObject.Lng;
            Active = clientObject.Active;
        }

        public ClientObject GetNewClientObjectFromModel()
        {
            return new ClientObject
            {
                Name = Name,
                WebAddress = WebAddress,
                Description = Description,
                Country = Country,
                Address = Address,
                Lat = Lat,
                Lng = Lng,
                Active = Active
            };
        }

        public void UpdateClientObjectFromModel(ClientObject clientObject)
        {
            clientObject.Name = Name;
            clientObject.WebAddress = WebAddress;
            clientObject.Description = Description;
            clientObject.Country = Country;
            clientObject.Address = Address;
            clientObject.Lat = Lat;
            clientObject.Lng = Lng;
            clientObject.Active = Active;
        }
    }
}
