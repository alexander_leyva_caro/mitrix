﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Mitrix.Data.Seeds
{
    public static class SeedRoles
    {
        public const string ADMIN_ROLE = "Admin";
        public const string CLIENT_ROLE = "Client";

        public static async Task Seed(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            // set admin role
            foreach (var roleName in new [] { ADMIN_ROLE, CLIENT_ROLE })
            {
                var role = await roleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    await roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }
        }
    }
}
