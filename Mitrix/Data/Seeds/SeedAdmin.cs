﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Mitrix.Infraestructure.Services.EmailSender;
using Mitrix.Models;

namespace Mitrix.Data.Seeds
{
    public static class SeedAdmin
    {
        public static async Task Seed(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var adminConfig = serviceProvider.GetRequiredService<IOptions<AdminConfig>>().Value;

            ApplicationUser admin = await userManager.FindByNameAsync(adminConfig.UserName);
            if (admin == null)
            {
                await userManager.CreateAsync(new ApplicationUser
                {
                    UserName = adminConfig.UserName,
                    Email = adminConfig.Email,
                    Address = adminConfig.Address,
                    PhoneNumber = adminConfig.PhoneNumber,
                    Company = adminConfig.Company,
                    Name = adminConfig.Name
                }, adminConfig.Password);

                admin = await userManager.FindByNameAsync(adminConfig.UserName);
            }

            // set admin role
            var adminRole = await roleManager.FindByNameAsync(SeedRoles.ADMIN_ROLE);
            if (adminRole == null)
            {
                await roleManager.CreateAsync(new IdentityRole(SeedRoles.ADMIN_ROLE));
            }
            await userManager.AddToRoleAsync(admin, SeedRoles.ADMIN_ROLE);
        }
    }
}
