﻿using System.Collections.Generic;
using System.Linq;

namespace Mitrix.Data.Repositories
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> FetchAll();

        T Find(object id);

        void Create(T entity);

        void Update(T entity);

        void Delete(T entity);
    }
}
