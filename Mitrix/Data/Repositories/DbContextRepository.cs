﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Mitrix.Data.DbContexts;

namespace Mitrix.Data.Repositories
{
    public class DbContextRepository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _dbContext;

        protected DbSet<T> DbSet => _dbContext.Set<T>();

        public DbContextRepository(DbContext dbContext, MitrixIdentityDbContext a)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<T> FetchAll()
        {
            return DbSet;
        }

        public T Find(object id)
        {
            return DbSet.Find(id);
        }

        public void Update(T entity)
        {
            DbSet.Update(entity);
            _dbContext.SaveChanges();
        }

        public void Create(T entity)
        {
            DbSet.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
            _dbContext.SaveChanges();
        }
    }
}
