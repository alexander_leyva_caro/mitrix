﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Mitrix.Data.Extensions;
using Mitrix.Models;

namespace Mitrix.Data.DbContexts
{
    public class MitrixIdentityDbContext : IdentityDbContext<ApplicationUser>
    {
        public MitrixIdentityDbContext(DbContextOptions dbContextOptions)
            : base(dbContextOptions)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            var types = typeof(EntityTypeConfiguration<>).GetTypeInfo().Assembly.GetTypes();
            var typesToRegister = types
                .Where(type => !string.IsNullOrEmpty(type.Namespace) &&
                               type.GetTypeInfo().BaseType != null &&
                               type.GetTypeInfo().BaseType.GetTypeInfo().IsGenericType &&
                               type.GetTypeInfo().BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));

            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                ModelBuilderExtensions.AddConfiguration(builder, configurationInstance);
            }

            base.OnModelCreating(builder);
        }

        public DbSet<ContractType> ContractTypes { get; set; }
    }
}
