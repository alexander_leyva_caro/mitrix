﻿using Microsoft.AspNetCore.Mvc;

namespace Mitrix.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            return View();
        }

        public ViewResult Error() => View();
    }
}
