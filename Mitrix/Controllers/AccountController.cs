﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Mitrix.Areas.Client.Controllers;
using Mitrix.Infraestructure.Services.EmailSender;
using Mitrix.Models;
using Mitrix.Models.ApplicationUserViewModels;

namespace Mitrix.Controllers
{
    public class AccountController : AppController
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IPasswordValidator<ApplicationUser> _passwordValidator;
        private readonly IEmailSender _emailSender;

        public AccountController(
            UserManager<ApplicationUser> userManager, 
            SignInManager<ApplicationUser> signInManager,
            IPasswordValidator<ApplicationUser> passwordValidator,
            IEmailSender emailSender)
            : base(userManager)
        {
            _signInManager = signInManager;
            _passwordValidator = passwordValidator;
            _emailSender = emailSender;
        }

        [AllowAnonymous]
        public IActionResult Login(string returnUrl)
        {
            return View(new LoginViewModel
            {
                ReturnUrl = returnUrl
            });
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginModel)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(loginModel.Email);
                if (user != null)
                {
                    await _signInManager.SignOutAsync();
                    var identityResult = await _signInManager.PasswordSignInAsync(user, loginModel.Password, false, false);

                    if (identityResult.Succeeded)
                        return await RedirectToLocal(loginModel.ReturnUrl, user);
                }
                ModelState.AddModelError("", "Invalid email or password");
            }
            return View(loginModel);
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToHome();
        }

        [AllowAnonymous]
        public ViewResult ForgotPassword()
        {
            return View(new ForgotPasswordViewModel());
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            model.Completed = false;

            if (!ModelState.IsValid)
                return View(model);

            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                ModelState.AddModelError("", "The email is not registered.");
                return View(model);
            }

            ViewBag.PasswordResetLink = PasswordResetLink(user, SetupPasswordType.ForgotPassword, _emailSender);
            model.Completed = true;
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult SetupPassword(string token, SetupPasswordType type, string email)
        {
            return View(new SetupPasswordViewModel
            {
                Token = token,
                Email = email,
                SetupPasswordType = type
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SetupPassword(SetupPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                ModelState.AddModelError("", "The email setup passwor link is incorrect. Please try the link again.");
                return View(model);
            }

            var passwordValidation = await _passwordValidator.ValidateAsync(UserManager, user, model.Password);
            if (passwordValidation.Succeeded)
            {
                var result = await UserManager.ResetPasswordAsync(user, model.Token, model.Password);
                if (result.Succeeded)
                {
                    return View(nameof(Login));
                }
                AddErrorsFromResult(result);
            }
            AddErrorsFromResult(passwordValidation);
            return View(model);
        }

        [AllowAnonymous]
        public IActionResult AccessDenied(string returnUrl)
        {
            return View((object) returnUrl);
        }
    }
}

