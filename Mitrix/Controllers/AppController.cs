﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mitrix.Areas.Admin.Controllers;
using Mitrix.Areas.Client.Controllers;
using Mitrix.Data.Seeds;
using Mitrix.Infraestructure.Services.EmailSender;
using Mitrix.Models;
using Mitrix.Models.ApplicationUserViewModels;

namespace Mitrix.Controllers
{
    public abstract class AppController : Controller
    {
        protected readonly UserManager<ApplicationUser> UserManager;

        protected AppController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        protected Task<ApplicationUser> CurrentUser => UserManager.GetUserAsync(HttpContext.User);

        protected void SetNotification(string notification)
        {
            //TempData["Notification"] = notification;
        }

        protected void AddErrorsFromResult(IdentityResult identityResult)
        {
            foreach (var error in identityResult.Errors)
                ModelState.AddModelError(string.Empty, error.Description);
        }

        protected virtual IActionResult RedirectToHome()
        {
            return RedirectToAction(nameof(HomeController.Index), GetControllerName<HomeController>());
        }

        protected virtual async Task<IActionResult> RedirectToLocal(string returnUrl, ApplicationUser currentUser = null)
        {
            if (returnUrl != null && Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            currentUser = currentUser ?? await CurrentUser;
            if (currentUser != null)
            {
                if (await UserManager.IsInRoleAsync(currentUser, SeedRoles.ADMIN_ROLE))
                {
                    return RedirectToAction(nameof(ClientController.Index), GetControllerName<ClientController>(),
                        new { area = SeedRoles.ADMIN_ROLE });
                }
                else
                {
                    return RedirectToAction(nameof(ProfileController.Index), GetControllerName<ProfileController>(),
                        new { area = SeedRoles.CLIENT_ROLE });
                }
                
            }
            return RedirectToHome();
        }

        protected string GetControllerName<T>()
        {
            return typeof(T).Name.Replace("Controller", "");
        }

        protected async Task<string> PasswordResetLink(ApplicationUser user, SetupPasswordType type, IEmailSender emailSender)
        {
            var passwordResetToken = await UserManager.GeneratePasswordResetTokenAsync(user);
            var linkToSetupPass = Url.Action(
                nameof(AccountController.SetupPassword), 
                GetControllerName<AccountController>(),
                new
                {
                    area = "",
                    token = passwordResetToken,
                    type,
                    email = user.Email
                },
                Request.Scheme);
            var message = type == SetupPasswordType.NewClient
                ? $"You are a new mitrix client. Please <a href=\"{linkToSetupPass}\">set up</a> your account."
                : $"Please recover your password by clicking <a href=\"{linkToSetupPass}\">here</a>";
            await emailSender.SendEmailAsync(user.Email, "Reset Password", message);
            return message;
        }

        protected async Task<IEnumerable<ApplicationUser>> GetClients()
        {
            var users = UserManager.Users
                .Include(user => user.ClientObjects)
                .Include(user => user.Contracts)
                .ThenInclude(contract => contract.ContractType);

            var result = new List<ApplicationUser>();
            foreach (var item in users)
            {
                if (await UserManager.IsInRoleAsync(item, SeedRoles.CLIENT_ROLE))
                    result.Add(item);
            }
            return result;
        }

        protected async Task<ApplicationUser> GetClient(string id)
        {
            return (await GetClients())
                .FirstOrDefault(item => item.Id == id);
        }
    }
}
