﻿//
var map;
var geocoder;
var autocomplete;
var infowindow;


// EVENTS:

// find the location in map
document.getElementById('geolocation-address-btn').addEventListener('click', function () {
    var address = document.getElementById('Address').value;
    geocodeAddress(geocoder, map, address, function(lat, lng) {
        document.getElementById("Lat").value = lat;
        document.getElementById("Lng").value = lng;
    });
});

// find the address in map
document.getElementById('geolocation-location-btn').addEventListener('click', function () {
    var lat = document.getElementById('Lat').value;
    var lng = document.getElementById('Lng').value;
    geocodeLocation(geocoder, map, infowindow, lat, lng, function (address) {
        document.getElementById("Address").value = address;
    });
});


// FUNCTIONS

// init
function initGeoLocation() {
    var latEle = document.getElementById('Lat');
    var lngEle = document.getElementById('Lng');
    map = new google.maps.Map(document.getElementById('geolocation-map'), {
        center: {
            lat: parseFloat(latEle.value || latEle.innerText),
            lng: parseFloat(lngEle.value || lngEle.innerText)
        },
        zoom: 8
    });
    geocoder = new google.maps.Geocoder();
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('Address')),
        { types: ['geocode'] }
    );
    infowindow = new google.maps.InfoWindow;
}

// locate the map into the specified address using the geocoder object
function geocodeAddress(geocoder, map, address, callBack) {
    geocoder.geocode({ 'address': address }, function (results, status) {
        console.log(results);
        if (status === "OK") {
            var location = results[0].geometry.location;

            map.setCenter(location);
            new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });

            callBack(location.lat(), location.lng());
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

// locate the map into the specified location using the geocode object
function geocodeLocation(geocoder, map, infowindow, lat, lng, callBack) {
    var location = { lat: parseFloat(lat), lng: parseFloat(lng) };
    geocoder.geocode({ 'location': location }, function (results, status) {
        if (status === 'OK') {
            if (results[1]) {
                var address = results[1].formatted_address;
                
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                infowindow.setContent(address);
                infowindow.open(map, marker);

                callBack(address);
            } else {
                window.alert('No results found');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}