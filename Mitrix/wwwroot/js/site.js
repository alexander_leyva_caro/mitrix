$(document).ready(function () {
    //$('#datatable-list-contracts').DataTable({
    //    searching: false,
    //    columnDefs: [
    //        { orderable: false, targets: -1 }
    //    ]
    //});

    //$('#datatable-list-clients').DataTable({
    //    columnDefs: [
    //        { orderable: false, targets: -1 }
    //    ]
    //});

    //$('#datatable-list-client-contracts').DataTable({
    //    columnDefs: [
    //        { orderable: false, targets: -1 }
    //    ]
    //});

    //$("table.datatable").DataTable({
    //    columnDefs: [
    //        { orderable: false, targets: -1 }
    //    ]
    //});

    $("table.datatable.datatable-no-search.datatable-actions").DataTable({
        searching: false,
        columnDefs: [
            { orderable: false, targets: -1 }
        ]
    });
    $("table.datatable.datatable-no-search.datatable-no-actions").DataTable({
        searching: false,
    });
    $("table.datatable.datatable-search.datatable-actions").DataTable({
        columnDefs: [
            { orderable: false, targets: -1 }
        ]
    });
    $("table.datatable.datatable-search.datatable-no-actions").DataTable({});

    $('#datetimepicker1').datetimepicker({
        //format: 'DD/MM/YYYY'
    });
    
    $(".input-group.datetimepicker").datetimepicker({
        //format: "MM/DD/YYYY",
        useCurrent: false,
        showClear: true,
        showTodayButton: true,
        allowInputToggle: true
    });

    var now = moment();
    var ranges = {
        "This Week": [
            now,
            moment(now).add(7, "d")
        ],
        "This Month": [
            now,
            moment(now).add(1, "M")
        ]
    };
    $("input.daterange:not(.daterange-custom)").daterangepicker({
        startDate: now,
        endDate: moment(now).add(1, "d"),
        ranges: ranges,
        minDate: now
    });
    $("input.daterange.daterange-custom").daterangepicker({
        //startDate: now,
        //endDate: moment(now).add(1, "d"),
        ranges: ranges,
        minDate: now
    });
});



