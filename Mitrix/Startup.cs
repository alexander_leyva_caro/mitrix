﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Mitrix.Data.DbContexts;
using Mitrix.Data.Repositories;
using Mitrix.Data.Seeds;
using Mitrix.Infraestructure.Services.EmailSender;
using Mitrix.Models;

namespace Mitrix
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment hostingEnvironment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(hostingEnvironment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("appsettings.email.json", optional: false, reloadOnChange: true)
                .AddJsonFile("appsettings.seeds.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{hostingEnvironment.EnvironmentName}.json", optional: true);

            if (hostingEnvironment.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("en-US");
                options.SupportedCultures = new List<CultureInfo> { new CultureInfo("en-US") };
                options.RequestCultureProviders.Clear();
            });

            // configure db context conection string
            services.AddDbContext<MitrixIdentityDbContext>(dbContextOptions =>
            {
                dbContextOptions.UseSqlServer(Configuration["ConnectionStrings:DefaultConnection"]);
            });

            // specify classes to manage app user and roles for identity
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<MitrixIdentityDbContext>()
                .AddDefaultTokenProviders();

            // Options
            services.Configure<GmailConfig>(gmailConfig => { Configuration.GetSection("GmailConfig").Bind(gmailConfig); });
            services.Configure<SendGridConfig>(sendGridConfig => { Configuration.GetSection("SendGridConfig").Bind(sendGridConfig); });
            services.Configure<AdminConfig>(adminConfig => { Configuration.GetSection("Admin").Bind(adminConfig); });

            // Add application services.
            services.AddTransient<DbContext, MitrixIdentityDbContext>();
            services.AddTransient<IRepository<ContractType>, DbContextRepository<ContractType>>();
            services.AddTransient<IRepository<Contract>, DbContextRepository<Contract>>();
            services.AddTransient<IRepository<ClientObject>, DbContextRepository<ClientObject>>();
            services.AddTransient<IEmailSender, GmailSender>();
            // services.AddTransient<IEmailSender, SendGridEmailSender>();

            services.AddMvc();
            services.AddDistributedMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseRequestLocalization();

            // loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseSession();
            app.UseStatusCodePages();
            app.UseStaticFiles();
            app.UseIdentity();
            app.UseMvc(configureRoutes =>
            {
                configureRoutes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                configureRoutes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseMvcWithDefaultRoute();

            // seeds
            SeedRoles.Seed(app.ApplicationServices).Wait();
            SeedAdmin.Seed(app.ApplicationServices).Wait();
        }
    }
}
