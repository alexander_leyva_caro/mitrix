﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Mitrix.Data.Repositories;
using Mitrix.Models;

namespace Mitrix.Infraestructure.ValidationAttributes
{
    public class DateRangeValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var valueAsString = value?.ToString();
            if (valueAsString == null)
                return new ValidationResult("Required");

            var dateRange = ParseDateRange(valueAsString);
            if (dateRange == null)
                return Error(validationContext);

            return dateRange.Item1 > dateRange.Item2 ? new ValidationResult("Invalid Range") : ValidationResult.Success;
        }

        protected ValidationResult Error(ValidationContext validationContext)
        {
            var message = FormatErrorMessage(validationContext.DisplayName);
            return new ValidationResult(message);
        }

        public static Tuple<DateTime, DateTime> ParseDateRange(string dateRange)
        {
            var splits = dateRange?.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            if (splits?.Length != 2)
                return null;

            if (!DateTime.TryParse(splits[0], new DateTimeFormatInfo(), DateTimeStyles.AssumeUniversal, out var a) 
                || !DateTime.TryParse(splits[1], new DateTimeFormatInfo(), DateTimeStyles.AssumeUniversal, out var b))
                return null;

            return new Tuple<DateTime, DateTime>(a, b);
        }
    }
}
