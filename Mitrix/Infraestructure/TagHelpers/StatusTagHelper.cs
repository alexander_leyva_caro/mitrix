﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Mitrix.Infraestructure.TagHelpers
{
    [HtmlTargetElement(Attributes = "mit-status")]
    public class StatusTagHelper : TagHelper
    {
        [HtmlAttributeName("mit-status")]
        public bool Status { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.SetAttribute("class", Status ? "fa fa-check" : "fa fa-ban");
        }
    }
}
