﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Mitrix.Infraestructure.TagHelpers
{
    [HtmlTargetElement(Attributes = "mit-active-route-class")]
    public class ActiveRouteClassTagHelper : TagHelper
    {
        [HtmlAttributeName("mit-active-route-class")]
        public string ActiveRouteClasses { get; set; }

        [HtmlAttributeName("asp-controller")]
        public string ControllerName { get; set; }

        [HtmlAttributeName("asp-action")]
        public string ActionName { get; set; }

        [HtmlAttributeName("mit-active-route-controllers")]
        public string ControllersName { get; set; }

        [HtmlAttributeName("mit-active-route-actions")]
        public string ActionsName { get; set; }

        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var activeController = IsActive(ControllerName, ControllersName, ViewContext.RouteData.Values["controller"].ToString());
            var activeAction = IsActive(ActionName, ActionsName, ViewContext.RouteData.Values["action"].ToString());

            if (activeController && activeAction)
            {
                output.Attributes.SetAttribute("class", ActiveRouteClasses);
            }
        }

        private static bool IsActive(string objectName, string objectNames, string objectType)
        {
            var isActive = false;
            var objects = new[] { objectName ?? "" };
            if (objectNames != null)
            {
                objects = objects.Concat(objectNames.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(item => item.Trim()))
                    .ToArray();
            }
            for (var i = 0; i < objects.Length && !isActive; i++)
            {
                isActive = (objects[i] ?? "").Equals(objectType, StringComparison.OrdinalIgnoreCase);
            }
            return isActive;
        }
    }
}
