﻿using System;

namespace Mitrix.Infraestructure.Extensions
{
    public static class DateTimeExtension
    {
        public static bool IsInRange(this DateTime dateTime, DateTime a, DateTime b)
        {
            return dateTime >= a && dateTime <= b;
        }
    }
}
