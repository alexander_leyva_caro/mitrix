﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Mitrix.Infraestructure.ActionFilterAttributes
{
    public class RestoreModelStateFromTempDataAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            //var controller = (Controller) filterContext.Controller;
            //if (controller.TempData.ContainsKey("ModelState"))
            //    controller.ModelState.Merge((ModelStateDictionary) controller.TempData["ModelState"]);
        }
    }
}
