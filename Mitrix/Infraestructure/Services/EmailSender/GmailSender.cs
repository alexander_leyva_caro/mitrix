﻿using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;

namespace Mitrix.Infraestructure.Services.EmailSender
{
    public class GmailSender : IEmailSender
    {
        private readonly GmailConfig _gmailConfig;

        public GmailSender(IOptions<GmailConfig> gmailConfig)
        {
            _gmailConfig = gmailConfig.Value;
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            var msg = new MimeMessage();
            msg.From.Add(new MailboxAddress("Admin Mitrix", _gmailConfig.Email));
            msg.To.Add(new MailboxAddress(email));
            msg.Subject = subject;

            var bodyBuilder = new BodyBuilder
            {
                HtmlBody = message,
                TextBody = message
            };
            msg.Body = bodyBuilder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 587);

                client.AuthenticationMechanisms.Remove("XOAUTH2");

                client.Authenticate(_gmailConfig.Email, _gmailConfig.Password);

                client.Send(msg);
                client.Disconnect(true);
                return Task.CompletedTask;
            }
        }
    }
}
