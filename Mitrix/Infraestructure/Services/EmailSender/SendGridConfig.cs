﻿namespace Mitrix.Infraestructure.Services.EmailSender
{
    public class SendGridConfig
    {
        public string Email { get; set; }
        public string ApiKey { get; set; }
    }
}
