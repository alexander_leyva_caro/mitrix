﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Mitrix.Infraestructure.Services.EmailSender
{
    public class SendGridEmailSender : IEmailSender
    {
        private readonly SendGridConfig _sendGridConfig;

        public SendGridEmailSender(IOptions<SendGridConfig> sendGridConfig)
        {
            _sendGridConfig = sendGridConfig.Value;
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            var sendGridClient = new SendGridClient(_sendGridConfig.ApiKey);
            var msg = new SendGridMessage
            {
                From = new EmailAddress(_sendGridConfig.Email),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };
            msg.AddTo(new EmailAddress(email));
            return sendGridClient.SendEmailAsync(msg);
        }
    }
}
