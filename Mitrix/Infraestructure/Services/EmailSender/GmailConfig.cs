﻿namespace Mitrix.Infraestructure.Services.EmailSender
{
    public class GmailConfig
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
