﻿using System.Threading.Tasks;

namespace Mitrix.Infraestructure.Services.EmailSender
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
