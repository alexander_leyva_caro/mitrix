﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Mitrix.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Company { get; set; }

        [Required]
        public string Address { get; set; }

        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }
        
        public bool Active { get; set; }

        public IEnumerable<Contract> Contracts { get; set; }

        public IEnumerable<ClientObject> ClientObjects { get; set; }
    }
}
