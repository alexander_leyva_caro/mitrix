﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Mitrix.Controllers;

namespace Mitrix.Models.ApplicationUserViewModels
{
    public class SetupPasswordViewModel : IValidatableObject
    {
        public string Token { get; set; }
        public string Email { get; set; }
        public SetupPasswordType SetupPasswordType { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DisplayName("Confirm Password")]
        [DataType(DataType.Password)]
        [Compare(nameof(Password))]
        public string PasswordConfirmation { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Token == null || Email == null)
            {
                yield return new ValidationResult("There was an error with your link to set up your password. Please try the link again.");
            }
            yield return ValidationResult.Success;
        }
    }
}
