﻿using System.ComponentModel.DataAnnotations;

namespace Mitrix.Models
{
    public class ClientObject
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Url]
        public string WebAddress { get; set; }

        public string Description { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public double Lat { get; set; }

        [Required]
        public double Lng { get; set; }

        public bool Active { get; set; }

        [Required]
        public string ClientId { get; set; }
        public ApplicationUser Client { get; set; }
    }
}
