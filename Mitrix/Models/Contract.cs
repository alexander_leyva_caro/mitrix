﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Mitrix.Infraestructure.Extensions;

namespace Mitrix.Models
{
    public class Contract : IValidatableObject
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [DisplayName("Contract Type")]
        public ContractType ContractType { get; set; }
        public string ContractTypeId { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayName("End Date")]
        public DateTime EndDate { get; set; }

        [Required]
        public ApplicationUser Client { get; set; }
        public string ClientId { get; set; }

        public bool Active { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // start data < end date
            if (StartDate >= EndDate)
            {
                yield return new ValidationResult("End Date must be after before Date", new[] { nameof(EndDate) });
            }

            // not overlapping another contract
            if (SomeContractOverlappingDates(Client.Contracts.Where(item =>item != this), StartDate, EndDate))
            {
                yield return new ValidationResult("Contract period overlapp another Contract");
            }
        }

        public static bool SomeContractOverlappingDates(IEnumerable<Contract> contracts, DateTime startDate, DateTime endDate)
        {
            return contracts.Any(contract =>
                startDate.IsInRange(contract.StartDate, contract.EndDate)
                 || endDate.IsInRange(contract.StartDate, contract.EndDate));
        }
    }
}
