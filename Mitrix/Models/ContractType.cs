﻿using System.ComponentModel.DataAnnotations;

namespace Mitrix.Models
{
    public class ContractType
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        public bool Active { get; set; }
    }
}
