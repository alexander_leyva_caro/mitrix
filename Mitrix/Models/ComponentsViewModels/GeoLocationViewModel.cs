﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mitrix.Models.ComponentsViewModels
{
    public class GeoLocationViewModel
    {
        [Required]
        public string Address { get; set; }

        [Required]
        public double Lat { get; set; }

        [Required]
        public double Lng { get; set; }

        public bool ReadOnly { get; set; }
    }
}
