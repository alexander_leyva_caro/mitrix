﻿using Microsoft.AspNetCore.Mvc;
using Mitrix.Models.ComponentsViewModels;

namespace Mitrix.Components
{
    public class GeoLocationViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(GeoLocationViewModel model)
        {
            return View(model);
        }
    }
}
